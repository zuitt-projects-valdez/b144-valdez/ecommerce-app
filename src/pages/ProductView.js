import React, { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col, Form } from "react-bootstrap";
import { useParams, useHistory, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function ProductView() {
  const { productId } = useParams();
  const { user } = useContext(UserContext);
  const history = useHistory();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);
  const token = localStorage.getItem("token");

  useEffect(() => {
    fetch(`https://boiling-earth-61174.herokuapp.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]); //changes in the product id will trigger the useEffect to set the information on the state

  const addToCart = () => {
    fetch(`https://boiling-earth-61174.herokuapp.com/users/addToCart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        productId: productId,
        numberOfItems: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === false) {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: "An error occurred. Please try again",
          });
        } else {
          Swal.fire({
            title: "Successfully Added to Cart",
            icon: "success",
            text: "Yay!",
          });
          console.log(data);
        }
      });
  };

  const productQuantity = (click) => {
    if (click === "subtract") {
      if (quantity !== 1) {
        setQuantity((count) => count - 1);
      }
    } else {
      setQuantity((count) => count + 1);
    }
  };
  return (
    <Container fluid className="bg-main full-vh">
      <h1 className="text-center pt-5">Products</h1>
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>Php {price}</Card.Text>
              <div className="mb-2">
                <Button
                  variant="secondary"
                  type="submit"
                  className="square text-white"
                  onClick={() => productQuantity("subtract")}
                >
                  -
                </Button>
                <Form.Label className="block mx-3">{quantity}</Form.Label>
                <Button
                  variant="secondary"
                  type="submit"
                  className="square text-white"
                  onClick={() => productQuantity("add")}
                >
                  +
                </Button>
                {user.id !== null ? (
                  user.isAdmin !== true ? (
                    <Button variant="primary" block onClick={() => addToCart()}>
                      Add to Cart
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      block
                      // onClick={() => enroll(courseId)}
                      disabled
                    >
                      Non-admin ability only
                    </Button>
                  )
                ) : (
                  <Link className="btn btn-danger btn-block" to="/login">
                    Login to Purchase
                  </Link>
                )}
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
