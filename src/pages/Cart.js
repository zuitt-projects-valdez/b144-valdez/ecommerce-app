import { Form, Button, Container } from "react-bootstrap";
import { Fragment, useState, useEffect, useContext } from "react";
import { useHistory, Link } from "react-router-dom";
import Swal from "sweetalert2";
import { Table } from "react-bootstrap";
import UserContext from "../UserContext";
import CartRow from "../components/CartRow";

export default function Cart() {
  //const { cartItems, onAdd, onRemove } = props;

  // const itemId = localStorage.getItem("productId");
  // const itemName = localStorage.getItem("productName");
  // const itemPrice = Number(localStorage.getItem("productPrice"));
  // const [amount, setAmount] = useState(1.0);
  const [total, setTotal] = useState(0);
  const { cart, setCart, refreshCart, updateCart } = useContext(UserContext);

  let itemPrice = [];

  //for array.reduce - returns sum of all elements in array
  const reducer = (value1, value2) => Number(value1) + Number(value2);

  const history = useHistory();
  // const amountNum = Number(amount);
  // // setTotal(itemPrice * amount);
  // useEffect(() => {
  //   setTotal(itemPrice * amountNum);
  // }, [total]);

  //bearer token variable
  const token = localStorage.getItem("token");

  useEffect(() => {
    if (refreshCart === true) {
      fetchCart();
    }
  }, [cart, refreshCart]);

  useEffect(() => {
    updateCart(true);
  }, []);

  //count total price
  const totalPrice = (data) => {
    data.forEach((product, index) => {
      //push total price for each item into an array
      itemPrice.push(product.price * product.numberOfItems);
    });
    if (itemPrice.length !== 0) {
      //get total order price
      setTotal(itemPrice.reduce(reducer));
    }
  };
  //fetching cart from User details

  const fetchCart = () => {
    fetch(`https://boiling-earth-61174.herokuapp.com/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((user) => {
        console.log(user);
        let data = user.orders;
        let info = data;
        console.log(info);
        totalPrice(info);
        cartRow(info);
      });
  };

  //set items to cart rows
  const cartRow = (data) => {
    setCart(
      data.map((product, index) => {
        return <CartRow key={product._id} cartProp={product} idx={index} />;
      })
    );
    updateCart(false);
  };

  //checkout function
  const checkout = () => {
    fetch(`https://boiling-earth-61174.herokuapp.com/users/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      // body: JSON.stringify({
      //   productId: itemId,
      //   amount: amountNum,
      // }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: "Order placed",
            icon: "success",
            text: "Thank you!",
          });
          setTotal(0);
          setCart([]);
          itemPrice = [];
          history.push("/order-history");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again",
          });
        }
      });
  };

  // console.log(typeof amountNum);
  // console.log(itemId);
  // console.log(amountNum);
  // console.log(total);

  return (
    <Container fluid className="full-vh bg-main">
      <h1 className="text-center py-5">Cart</h1>
      <div className="width-80 mx-auto bg-white-transparent">
        {total === 0 ? (
          <Fragment>
            <h2>Your cart is empty! Let's change that 😉</h2>
            <Link className="btn btn-primary btn-block" to="/products">
              Go to Products
            </Link>
          </Fragment>
        ) : (
          <Fragment>
            <Table
              striped
              bordered
              hover
              size="sm"
              className="my-5 border border-dark"
            >
              <thead>
                <tr className="text-center">
                  <th>Product Name</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Subtotal</th>
                  <th>Remove</th>
                </tr>
              </thead>
              <tbody>{cart}</tbody>
            </Table>
            <div className="d-flex flex-column align-items-end">
              <Form.Label>Total: Php {total}</Form.Label>
              <br />
              <Button
                variant="primary"
                type="submit"
                onClick={() => checkout()}
              >
                Place order
              </Button>
            </div>
          </Fragment>
        )}
      </div>
    </Container>
  );
}
