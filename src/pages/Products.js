import { Fragment, useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import ProductCard from "../components/ProductCard";

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`https://boiling-earth-61174.herokuapp.com/products/`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProducts(
          data.map((product) => {
            return <ProductCard key={product._id} productProp={product} />;
          })
        );
      });
  }, []);

  return (
    <Container fluid className="bg-main full-vh">
      <h1 className="text-center pt-5">Products</h1>
      <div className="d-flex justify-content-around  p-5">
        <Fragment>{products}</Fragment>
      </div>
    </Container>
  );
}
