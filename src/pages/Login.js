import { Form, Button, Container } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Link, Redirect, useHistory } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Login(props) {
  const { user, setUser } = useContext(UserContext);

  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(true);

  function authenticate(e) {
    e.preventDefault();

    fetch(`https://boiling-earth-61174.herokuapp.com/users/login`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.accessToken !== "undefined") {
          //sets the user info into the application
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);
          Swal.fire({
            title: "Successfully logged in",
            icon: "success",
            text: "Welcome back!",
          });
          history.push("/products");
        } else {
          Swal.fire({
            // doesn't work
            title: "Error",
            icon: "error",
            text: "Please check login details and try again.",
          });
        }
      });
    //clears info once submitted
    setEmail("");
    setPassword("");
  }

  const retrieveUserDetails = (token) => {
    fetch(`https://boiling-earth-61174.herokuapp.com/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Redirect to="/products" />
  ) : (
    <Container fluid className="bg-logreg full-vh py-5">
      <div className="mx-auto width-50 rounded bg-white-transparent p-5 mt-5">
        <h1 className="mb-3 text-center">Login</h1>
        <Form onSubmit={(e) => authenticate(e)}>
          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              width="70%"
              required
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="password">
            <Form.Label className="mx-auto">Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>
          {isActive ? (
            <Button
              type="submit"
              id="submitBtn"
              variant="dark"
              className="d-block mt-4 px-4 py-2 mx-auto submit-btn pe-auto"
            >
              Submit
            </Button>
          ) : (
            <Button
              type="submit"
              id="submitBtn"
              variant="dark"
              className="d-block mt-4 px-4 py-2 mx-auto submit-btn pe-auto"
              disabled
            >
              Submit
            </Button>
          )}
        </Form>
        <Link to="/register" className="d-block text-center mt-2 register-link">
          New user? Register here
        </Link>
      </div>
    </Container>
  );
}
