import { Button, Container, Modal, Form } from "react-bootstrap";
import Table from "react-bootstrap/Table";
import { Fragment, useEffect, useState } from "react";
import ADashRow from "../components/ADashRow";
import Swal from "sweetalert2";

export default function OrderHistory() {
  const [row, setRow] = useState([]);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  const [showAdd, setShowAdd] = useState(false);
  //function to toggle opening and closing of modal
  const openAdd = () => setShowAdd(true);
  const closeAdd = () => setShowAdd(false);

  //function add course
  function addProduct(e) {
    e.preventDefault();

    fetch(`https://boiling-earth-61174.herokuapp.com/products/`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === false) {
          Swal.fire({
            title: "An error occurred",
            icon: "error",
            text: "Please try again.",
          });
        } else {
          Swal.fire({
            title: "Added successfully",
            icon: "success",
            text: "New product available",
          });
          setName("");
          setDescription("");
          setPrice(0);

          //close modal upon add
          closeAdd();
        }
      });
  }
  // edit product details function

  useEffect(() => {
    fetch(`https://boiling-earth-61174.herokuapp.com/products/all`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setRow(
          data.map((row) => {
            //check if fetched
            return <ADashRow key={row._id} rowProp={row} />;
          })
        );
      });
  }, []);

  return (
    <Container fluid className="full-vh">
      <h1 className="text-center pt-5">Admin Dashboard</h1>
      <div className="d-flex justify-content-center my-3">
        <Button variant="primary" onClick={openAdd}>
          Add Product
        </Button>
      </div>
      <Table hover bordered>
        <thead>
          <tr>
            <th>ID</th>
            <th>Product Name</th>
            <th>Description</th>
            <th>Price</th>
            {/* <th>Availability</th> */}
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <Fragment>{row}</Fragment>
        </tbody>
      </Table>

      {/* Add modal */}
      <Modal show={showAdd} onHide={closeAdd}>
        <Form onSubmit={(e) => addProduct(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Add Course</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="courseName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="courseDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="coursePrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                required
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={closeAdd}>
              Close
            </Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </Container>
  );
}
