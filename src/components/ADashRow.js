import { Fragment } from "react";
import { Link } from "react-router-dom";

export default function ADashRow({ rowProp }) {
  const { _id, name, description, price } = rowProp;

  return (
    <Fragment>
      <tr>
        <td>{_id}</td>
        <td>{name}</td>
        <td>{description}</td>
        <td>{price}</td>
        <td>
          <Link
            className="btn btn-danger btn-block mx-auto"
            to={`/editproduct/${_id}`}
          >
            Edit
          </Link>
        </td>
      </tr>
    </Fragment>
  );
}
