import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ProductCard({ productProp }) {
  // const { user } = useContext(UserContext);

  // const [cart, setCart] = useState([]);
  const { _id, name, price } = productProp;

  // const addToCart = (product) => {
  //   console.log(product);
  //   setCart([...cart, product]);
  //   localStorage.setItem("productId", productProp._id);
  //   localStorage.setItem("productName", productProp.name);
  //   localStorage.setItem("productPrice", productProp.price);
  // };

  return (
    <Card
      style={{ width: "18rem", height: "24rem" }}
      className=" my-5 text-center"
    >
      <Card.Body className="justify-content-center flex-column d-flex">
        <Card.Title className="text-uppercase">{name}</Card.Title>
        {/* <Card.Text>{description}</Card.Text> */}
        <Card.Text>PhP {price}</Card.Text>
        {/* {user.id !== null ? (
          user.isAdmin == false ? ( */}
        {/* <Link variant="primary" onClick={() => addToCart(productProp)}>
          Product Details
        </Link> */}
        <Link className="btn btn-primary btn-block" to={`/products/${_id}`}>
          Details
        </Link>
        {/* ) : (
            <Button variant="primary" disabled>
              Non-admin ability only
            </Button>
          )
        ) : (
          <Link className="btn btn-danger btn-block" to="/login">
            Login to Purchase
          </Link>
        )} */}
      </Card.Body>
    </Card>
  );
}
