import React, { Fragment } from "react";

export default function OrderRow({ orderRowProp }) {
  const { _id, totalAmount, purchasedOn } = orderRowProp;
  // const reducer = (value1, value2) => Number(value1) + Number(value2);
  // const [quantity, setQuantity] = useState(0);
  // const token = localStorage.getItem("token");
  // useEffect(() => {
  //   fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
  //     headers: {
  //       Authorization: `Bearer ${token}`,
  //     },
  //   })
  //     .then((res) => res.json())
  //     .then((user) => {
  //       setQuantity(user.orders.numberOfItems.reduce(reducer));
  //     });
  // }, []);
  return (
    <Fragment>
      <tr>
        <td>{_id}</td>
        <td>{purchasedOn}</td>
        {/* <td>{productName}</td>
        <td>{quantity}</td> */}
        <td>{totalAmount}</td>
      </tr>
    </Fragment>
  );
}
